package br.com.ufrn.imd.smartparking.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.PrimeFaces;

import com.google.gson.Gson;

import br.com.ufrn.imd.smartparking.dominio.Estacionamento;
import br.com.ufrn.imd.smartparking.dto.EstacionamentoDto;
import br.com.ufrn.imd.smartparking.dto.VagaDto;
import br.com.ufrn.imd.smartparking.dto.mapper.EstacionamentoMapper;
import br.com.ufrn.imd.smartparking.dto.mapper.VagaMapper;
import br.com.ufrn.imd.smartparking.service.EstacionamentoService;
import br.com.ufrn.imd.smartparking.service.VagaService;

@Named
@ViewScoped
public class IndexBean implements Serializable{

	private static final long serialVersionUID = 2295471882298285382L;
	
	@Inject
	private EstacionamentoService estacionamentoService;
	@Inject
	private VagaService vagaService;
	
	private List<Estacionamento> estacionamentos;
	private List<EstacionamentoDto> listDto;
	private List<VagaDto> vagasDto;
	private String estacionamentoSelecionado;
	
	@PostConstruct
	public void init() {
		estacionamentos = estacionamentoService.findAll();
		vagasDto = new ArrayList<>();
		listDto = EstacionamentoMapper.map(estacionamentos);
		estacionamentoSelecionado = "";
		PrimeFaces.current().executeScript("loadEstacionamento();");
		
	}
	
	public String getEstacionamentos() {
		return new Gson().toJson(listDto);
	}
	
	public String getVagas() {
		return new Gson().toJson(vagasDto);
	}
	
	public void setVagas(String vagas) {
		//return new Gson().toJson(vagasDto);
	}
	
	public String getEstacionamentoSelecionado() {
		return estacionamentoSelecionado;
	}

	public void setEstacionamentoSelecionado(String estacionamentoSelecionado) {
		this.estacionamentoSelecionado = estacionamentoSelecionado;
		
		EstacionamentoDto dto = new Gson().fromJson(estacionamentoSelecionado, EstacionamentoDto.class);
		vagasDto = VagaMapper.map(vagaService.findByEstacionamentoID(dto.getId()));
		PrimeFaces.current().executeScript("loadVagas();");
	}
	
}
