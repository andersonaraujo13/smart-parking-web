package br.com.ufrn.imd.smartparking.dominio;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "endereco", schema = "public")
public class Endereco implements Serializable {

	private static final long serialVersionUID = 3283164769386245824L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_endereco", unique = true, nullable = false)
	private int id;	
	
	@Column(name = "logradouro")
	private String logradouro;
	
	@Column(name = "complemento")
	private String complemento;
	
	@Column(name = "numero")
	private String numero;
	
	@Column(name = "cidade")
	private String cidade;
	
	@Column(name = "estado_extenso")
	private String estadoExtenso;
	
	@Column(name = "estado_sigla")
	private String estadoSigla;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstadoExtenso() {
		return estadoExtenso;
	}
	public void setEstadoExtenso(String estadoExtenso) {
		this.estadoExtenso = estadoExtenso;
	}
	public String getEstadoSigla() {
		return estadoSigla;
	}
	public void setEstadoSigla(String estadoSigla) {
		this.estadoSigla = estadoSigla;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}	
	public String getEnderecoExtenso() {
		if(logradouro.isEmpty() || cidade.isEmpty() || estadoExtenso.isEmpty()) {
			return "Não Informado";
		} else {
			return logradouro + ", " + cidade + "/" + estadoExtenso;
		}
	}
}
