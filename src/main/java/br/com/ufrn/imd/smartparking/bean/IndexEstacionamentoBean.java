package br.com.ufrn.imd.smartparking.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import br.com.ufrn.imd.smartparking.dominio.Estacionamento;
import br.com.ufrn.imd.smartparking.dominio.Usuario;
import br.com.ufrn.imd.smartparking.dominio.Vaga;
import br.com.ufrn.imd.smartparking.service.EstacionamentoService;
import br.com.ufrn.imd.smartparking.service.VagaService;
import br.com.ufrn.imd.smartparking.utils.Utils;

@Named
@ViewScoped
public class IndexEstacionamentoBean implements Serializable {

	private static final long serialVersionUID = -961990319754192231L;
	
	private Estacionamento estacionamento;
	private MapModel simpleModel;
	
	@Inject
	private EstacionamentoService estacionamentoService;
	@Inject
	private VagaService vagaService;
	
	@PostConstruct
	public void init() {
		simpleModel = new DefaultMapModel();
		loadMarkers();
		
	}

	public Estacionamento getEstacionamento() {
		return estacionamento;
	}

	public void setEstacionamento(Estacionamento estacionamento) {
		this.estacionamento = estacionamento;
	}

	public MapModel getSimpleModel() {
		return simpleModel;
	}

	public void setSimpleModel(MapModel simpleModel) {
		this.simpleModel = simpleModel;
	}
	
	public void loadMarkers() {
		Usuario user = (Usuario) Utils.getSession().getAttribute("usuario");
		List<Estacionamento> list = estacionamentoService.findEstacionamentosByUser(user);
		estacionamento = list.stream().findFirst().get();
		
		List<Vaga> vagas = vagaService.findByEstacionamento(estacionamento);
		vagas.stream().forEach(entity -> {
			LatLng testes = new LatLng(entity.getLatitude(), entity.getLongitude());
			Marker spot = new Marker(testes);
			if(new Date().getTime() % 2 == 0) {
				spot.setIcon("http://maps.google.com/mapfiles/ms/micons/red-dot.png");
			} else {
				spot.setIcon("http://maps.google.com/mapfiles/ms/micons/green-dot.png");
			}
			simpleModel.addOverlay(spot);
		});
	}
}
