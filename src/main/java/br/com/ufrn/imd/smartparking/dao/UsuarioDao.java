package br.com.ufrn.imd.smartparking.dao;

import java.util.Optional;


import br.com.ufrn.imd.smartparking.dominio.Usuario;

public class UsuarioDao extends GenericDao<Usuario>{

	public Optional<Usuario> autenticar(String email, String password) throws Exception {
		prepareBuildQuery();
		q.select(root);
		q.where(
				cb.and(
						cb.equal(root.get("login"), email), cb.equal(root.get("senha"), password)		
						)
				);
		return Optional.of(getInstance().createQuery(q).getSingleResult());
	}

}
