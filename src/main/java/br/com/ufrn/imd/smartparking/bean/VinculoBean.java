package br.com.ufrn.imd.smartparking.bean;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import br.com.ufrn.imd.smartparking.dominio.Instituicao;
import br.com.ufrn.imd.smartparking.dominio.Usuario;
import br.com.ufrn.imd.smartparking.dominio.Vinculo;
import br.com.ufrn.imd.smartparking.utils.Utils;

@Named
@ViewScoped
public class VinculoBean implements Serializable {

	private static final long serialVersionUID = 8147285916825561525L;
	
	public Vinculo getVinculo(Instituicao instituicao) {
		Usuario usuario = (Usuario) Utils.getSession().getAttribute("usuario");
		Optional<Vinculo> aux = Optional.empty();
		if(Objects.nonNull(usuario.getInstituicoes()) && !usuario.getInstituicoes().isEmpty()) {
			aux = usuario.getInstituicoes().stream().filter(vinculo -> vinculo.getInstituicao().equals(instituicao)).findFirst();
		}
		
		return aux.orElse(null);
	}
}
