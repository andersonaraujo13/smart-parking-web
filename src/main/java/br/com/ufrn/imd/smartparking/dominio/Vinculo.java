package br.com.ufrn.imd.smartparking.dominio;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vinculo", schema = "public")
public class Vinculo implements Serializable{
	
	
	private static final long serialVersionUID = 6067697168584363053L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_vinculo", unique = true, nullable = false)
	private int id;
	
	@OneToOne
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;
	
	@OneToOne
	@JoinColumn(name = "id_instituicao")
	private Instituicao instituicao;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "vinculo")
	private VinculoTipo vinculo;
	
	@Column(name = "ativo")
	private boolean ativo;
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Instituicao getInstituicao() {
		return instituicao;
	}
	public void setInstituicao(Instituicao instituicao) {
		this.instituicao = instituicao;
	}
	public VinculoTipo getVinculo() {
		return vinculo;
	}
	public void setVinculo(VinculoTipo vinculo) {
		this.vinculo = vinculo;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
