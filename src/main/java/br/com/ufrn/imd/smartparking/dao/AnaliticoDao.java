package br.com.ufrn.imd.smartparking.dao;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.hibernate.query.NativeQuery;
import org.hibernate.type.IntegerType;

import br.com.ufrn.imd.smartparking.dominio.Filtro;

@SuppressWarnings("unchecked")
@Stateless
public class AnaliticoDao {
	
	private static EntityManagerFactory entityManagerFactory;
	
	public Integer getTotalEstacionamento(Filtro filtro) {
		getInstance();
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query q = entityManager.createNativeQuery(
				"select \r\n" + 
				"	cast(count(he.id_historico_estacionamento)as int)\r\n" + 
				"from public.historico_estacionamento as he \r\n" + 
				"join public.vaga as v on v.id_vaga = he.id_vaga\r\n" + 
				"join public.estacionamento as e on e.id_estacionamento = v.id_estacionamento\r\n" + 
				"join public.instituicao as i on i.id_instituicao = e.id_instituicao\r\n" + 
				"where \r\n" + 
				"	(1=1) \r\n" + 
				"	and (i.id_instituicao = :instituicao or :instituicao is null) \r\n" + 
				"	and (e.id_estacionamento = :estacionamento or :estacionamento is null) \r\n"+
				"	and (he.data_entrada >= :dataInicio)\r\n" + 
				"	and (he.data_saida <= :dataFim)")
		.unwrap(NativeQuery.class)
		.setParameter("instituicao", Objects.isNull(filtro.getInstituicao()) ? null : filtro.getInstituicao().getId(), IntegerType.INSTANCE)
		.setParameter("estacionamento", Objects.isNull(filtro.getEstacionamento()) ? null : filtro.getEstacionamento().getId(), IntegerType.INSTANCE)
		.setParameter("dataInicio", filtro.getDataInicio())
		.setParameter("dataFim", filtro.getDataFim());
		
		Integer total = (Integer) q.getSingleResult();
		
		return total;
	}
	
	public Integer getTotalTempo(Filtro filtro) {
		getInstance();
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query q = entityManager.createNativeQuery(
				"select \r\n" + 
				"	cast(sum(extract(EPOCH FROM data_saida - data_entrada)) / 3600 as int) as total\r\n" + 
				"from public.historico_estacionamento as he \r\n" + 
				"join public.vaga as v on v.id_vaga = he.id_vaga\r\n" + 
				"join public.estacionamento as e on e.id_estacionamento = v.id_estacionamento\r\n" + 
				"join public.instituicao as i on i.id_instituicao = e.id_instituicao\r\n" + 
				"where \r\n" + 
				"	(1=1) \r\n" + 
				"	and (i.id_instituicao = :instituicao or :instituicao is null) \r\n" + 
				"	and (e.id_estacionamento = :estacionamento or :estacionamento is null) " + 
				"	and (he.data_entrada >= :dataInicio)\r\n" + 
				"	and (he.data_saida <= :dataFim)")
		.unwrap(NativeQuery.class)
		.setParameter("instituicao", Objects.isNull(filtro.getInstituicao()) ? null : filtro.getInstituicao().getId(), IntegerType.INSTANCE)
		.setParameter("estacionamento", Objects.isNull(filtro.getEstacionamento()) ? null : filtro.getEstacionamento().getId(), IntegerType.INSTANCE)
		.setParameter("dataInicio", filtro.getDataInicio())
		.setParameter("dataFim", filtro.getDataFim());
		
		Integer total = (Integer) q.getSingleResult();
		
		return total;
	}
	
	
	@Deprecated
	public Map<String, Integer> getTotalVagas(Integer instituicao, Integer estacionamento) {
		getInstance();
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query q = entityManager.createNativeQuery(
				"SELECT v.tipo_vaga, cast(count(v.id_vaga) as INT) FROM public.vaga as v\r\n" + 
				"join public.estacionamento as e on e.id_estacionamento = v.id_estacionamento\r\n" + 
				"join public.instituicao as i on i.id_instituicao = e.id_instituicao\r\n" + 
				"where (1=1) \r\n" + 
				"and (i.id_instituicao = :instituicao or :instituicao is null) \r\n" + 
				"and (e.id_estacionamento = :estacionamento or :estacionamento is null)\r\n" + 
				"group by v.tipo_vaga;\r\n");
		q.setParameter("instituicao", instituicao);
		q.setParameter("estacionamento", estacionamento);
		
		List<Object[]> list = q.getResultList();
		
		Map<String, Integer> map =  new HashMap<>();
		
		for(Object[] objects : list) {
			map.put((String)objects[0], (Integer)objects[1]);
		}	
		
		entityManager.close();
		return map;
	}
	
	@Deprecated
	public Map<String, Integer> getTotalOcupadas(Integer instituicao, Integer estacionamento) {
		getInstance();
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query q = entityManager.createNativeQuery(
				"SELECT v.tipo_vaga, cast(count(v.id_vaga) as INT) FROM public.vaga as v\r\n" + 
				"join public.estacionamento as e on e.id_estacionamento = v.id_estacionamento\r\n" + 
				"join public.instituicao as i on i.id_instituicao = e.id_instituicao\r\n" + 
				"where (1=1) \r\n" + 
				"and (i.id_instituicao = :instituicao or :instituicao is null) \r\n" + 
				"and (e.id_estacionamento = :estacionamento or :estacionamento is null) \r\n" + 
				"and v.ocupado is true \r\n" +
				"group by v.tipo_vaga;\r\n");
		q.setParameter("instituicao", instituicao);
		q.setParameter("estacionamento", estacionamento);
		
		List<Object[]> list = q.getResultList();
		
		Map<String, Integer> map =  new HashMap<>();
		
		for(Object[] objects : list) {
			map.put((String)objects[0], (Integer)objects[1]);
		}	
		
		entityManager.close();
		return map;
		
	}
	
	@Deprecated
	public Map<Integer, Integer> getSensoriamento(Integer instituicao, Integer estacionamento) {
		getInstance();
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query q = entityManager.createNativeQuery(
				"SELECT v.tipo_monitoramento, cast(count(v.id_vaga) as INT) FROM public.vaga as v\r\n" + 
				"join public.estacionamento as e on e.id_estacionamento = v.id_estacionamento\r\n" + 
				"join public.instituicao as i on i.id_instituicao = e.id_instituicao\r\n" + 
				"where (1=1) \r\n" + 
				"and (i.id_instituicao = :instituicao or :instituicao is null) \r\n" + 
				"and (e.id_estacionamento = :estacionamento or :estacionamento is null) \r\n" + 
				"group by v.tipo_monitoramento;\r\n");
		q.setParameter("instituicao", instituicao);
		q.setParameter("estacionamento", estacionamento);
		
		List<Object[]> list = q.getResultList();
		
		Map<Integer, Integer> map =  new HashMap<>();
		
		for(Object[] objects : list) {
			map.put((Integer)objects[0], (Integer)objects[1]);
		}	
		
		entityManager.close();
		return map;
	}
	
	public Map<Integer, BigInteger> getTempoMedioSemana(Filtro filtro) {
		getInstance();
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query q = entityManager.createNativeQuery(
				" with tempo as (\r\n" + 
				"	select \r\n" + 
				"		cast(to_char(data_entrada, 'D')as int) as dia, \r\n" + 
				"		cast(extract(EPOCH FROM data_saida - data_entrada) / 60 as bigint) as numero,\r\n" + 
				"		ROW_NUMBER() OVER (PARTITION BY to_char(data_entrada, 'D') ORDER BY cast(extract(EPOCH FROM data_saida - data_entrada) / 60 as bigint)) as id, \r\n" + 
				"		COUNT(he.id_historico_estacionamento) OVER (PARTITION BY to_char(data_entrada, 'D')) as total\r\n" + 
				"	from public.historico_estacionamento as he \r\n" + 
				"	join public.vaga as v on v.id_vaga = he.id_vaga\r\n" + 
				"	join public.estacionamento as e on e.id_estacionamento = v.id_estacionamento\r\n" + 
				"	join public.instituicao as i on i.id_instituicao = e.id_instituicao\r\n" + 
				"	where \r\n" + 
				"		(1=1) \r\n" + 
				"		and (i.id_instituicao = :instituicao or :instituicao is null) \r\n" + 
				"		and (e.id_estacionamento = :estacionamento or :estacionamento is null) \r\n" + 
				"		and (he.data_entrada >= :dataInicio)\r\n" + 
				"		and (he.data_saida <= :dataFim) \r\n" + 
				"	order by dia, numero\r\n" + 
				" )\r\n" + 
				"select dia, cast(avg(numero) as bigint) from tempo where id between (total + 1)/2 and (total + 2)/2 group by dia order by dia;")
		.unwrap(NativeQuery.class)
		.setParameter("instituicao", Objects.isNull(filtro.getInstituicao()) ? null : filtro.getInstituicao().getId(), IntegerType.INSTANCE)
		.setParameter("estacionamento", Objects.isNull(filtro.getEstacionamento()) ? null : filtro.getEstacionamento().getId(), IntegerType.INSTANCE)
		.setParameter("dataInicio", filtro.getDataInicio())
		.setParameter("dataFim", filtro.getDataFim());

		List<Object[]> list = q.getResultList();
		
		Map<Integer, BigInteger> map =  new HashMap<>();
		
		for(Object[] objects : list) {
			map.put((Integer)objects[0], (BigInteger)objects[1]);
		}	
		
		entityManager.close();
		return map;
	}
	
	public Map<Integer, BigInteger> getQuantidadeMediaEstacionamentoSemana(Filtro filtro) {
		getInstance();
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query q = entityManager.createNativeQuery(
				"with total as (select \r\n" + 
				"		id, dw, valor, COUNT(id) OVER (PARTITION BY dw) as total\r\n" + 
				"	from (select\r\n" + 
				"		ROW_NUMBER() OVER (PARTITION BY to_char(data_entrada, 'D') ORDER by count(he.id_historico_estacionamento)) as id,\r\n" + 
				"		to_char(data_entrada, 'D') as dw,\r\n" + 
				"		to_char(data_entrada, 'DD') as dia,\r\n" + 
				"		count(he.id_historico_estacionamento) as valor\r\n" + 
				"	from public.historico_estacionamento as he \r\n" + 
				"	join public.vaga as v on v.id_vaga = he.id_vaga\r\n" + 
				"	join public.estacionamento as e on e.id_estacionamento = v.id_estacionamento\r\n" + 
				"	join public.instituicao as i on i.id_instituicao = e.id_instituicao\r\n" + 
				"	where \r\n" + 
				"		(1=1) \r\n" + 
				"		and (i.id_instituicao = :instituicao or :instituicao is null) \r\n" + 
				"		and (e.id_estacionamento = :estacionamento or :estacionamento is null) \r\n" +
				"		and (he.data_entrada >= :dataInicio)\r\n" + 
				"		and (he.data_saida <= :dataFim) \r\n" +
				"	group by dw,dia\r\n" + 
				"	order by dw, dia, valor\r\n" + 
				"	) as query)\r\n" + 
				"	select cast(dw as int), cast(avg(valor) as bigint) from total where id between (total + 1)/2 and (total + 2)/2 group by dw order by dw")
		.unwrap(NativeQuery.class)
		.setParameter("instituicao", Objects.isNull(filtro.getInstituicao()) ? null : filtro.getInstituicao().getId(), IntegerType.INSTANCE)
		.setParameter("estacionamento", Objects.isNull(filtro.getEstacionamento()) ? null : filtro.getEstacionamento().getId(), IntegerType.INSTANCE)
		.setParameter("dataInicio", filtro.getDataInicio())
		.setParameter("dataFim", filtro.getDataFim());
					
		List<Object[]> list = q.getResultList();
		
		Map<Integer, BigInteger> map =  new HashMap<>();
		
		for(Object[] objects : list) {
			map.put((Integer)objects[0], (BigInteger)objects[1]);
		}	
		
		entityManager.close();
		return map;
	}
	public Map<Integer, Integer> getQuantidadeEstacionamentoTotalSemana(Filtro filtro) {
		getInstance();
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query q = entityManager.createNativeQuery(
				"select\r\n" + 
				"		cast(to_char(data_entrada, 'D') as int)as dw,\r\n" + 
				"		cast(count(he.id_historico_estacionamento) as int) as valor\r\n" + 
				"	from public.historico_estacionamento as he \r\n" + 
				"	join public.vaga as v on v.id_vaga = he.id_vaga\r\n" + 
				"	join public.estacionamento as e on e.id_estacionamento = v.id_estacionamento\r\n" + 
				"	join public.instituicao as i on i.id_instituicao = e.id_instituicao\r\n" + 
				"	where \r\n" + 
				"		(1=1) \r\n" + 
				"		and (i.id_instituicao = :instituicao or :instituicao is null) \r\n" + 
				"		and (e.id_estacionamento = :estacionamento or :estacionamento is null) \r\n" + 
				"		and (he.data_entrada >= :dataInicio)\r\n" + 
				"		and (he.data_saida <= :dataFim) \r\n" +
				"	group by dw\r\n" + 
				"	order by dw")
		.unwrap(NativeQuery.class)
		.setParameter("instituicao", Objects.isNull(filtro.getInstituicao()) ? null : filtro.getInstituicao().getId(), IntegerType.INSTANCE)
		.setParameter("estacionamento", Objects.isNull(filtro.getEstacionamento()) ? null : filtro.getEstacionamento().getId(), IntegerType.INSTANCE)
		.setParameter("dataInicio", filtro.getDataInicio())
		.setParameter("dataFim", filtro.getDataFim());
	
		List<Object[]> list = q.getResultList();
		
		Map<Integer, Integer> map =  new HashMap<>();
		
		for(Object[] objects : list) {
			map.put((Integer)objects[0], (Integer)objects[1]);
		}	
		
		entityManager.close();
		return map;
	}
	
	public Map<Integer, BigInteger> getTempoMedioMes(Filtro filtro) {
		getInstance();
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query q = entityManager.createNativeQuery(
				" with tempo as (\r\n" + 
				"	select \r\n" + 
				"		cast(to_char(data_entrada, 'MM') as int) as mes, \r\n" + 
				"		cast(extract(EPOCH FROM data_saida - data_entrada) / 60 as bigint) as numero,\r\n" + 
				"		ROW_NUMBER() OVER (PARTITION BY to_char(data_entrada, 'MM') ORDER BY cast(extract(EPOCH FROM data_saida - data_entrada) / 60 as bigint)) as id, \r\n" + 
				"		COUNT(he.id_historico_estacionamento) OVER (PARTITION BY to_char(data_entrada, 'MM')) as total\r\n" + 
				"	from public.historico_estacionamento as he \r\n" + 
				"	join public.vaga as v on v.id_vaga = he.id_vaga\r\n" + 
				"	join public.estacionamento as e on e.id_estacionamento = v.id_estacionamento\r\n" + 
				"	join public.instituicao as i on i.id_instituicao = e.id_instituicao\r\n" + 
				"	where \r\n" + 
				"		(1=1) \r\n" + 
				"		and (i.id_instituicao = :instituicao or :instituicao is null) \r\n" + 
				"		and (e.id_estacionamento = :estacionamento or :estacionamento is null) \r\n" + 
				"		and (he.data_entrada >= :dataInicio)\r\n" + 
				"		and (he.data_saida <= :dataFim) \r\n" +
				"	order by mes, numero\r\n" + 
				" )\r\n" + 
				"select mes, cast(avg(numero) as bigint) from tempo where id between (total + 1)/2 and (total + 2)/2 group by mes order by mes;")
		.unwrap(NativeQuery.class)
		.setParameter("instituicao", Objects.isNull(filtro.getInstituicao()) ? null : filtro.getInstituicao().getId(), IntegerType.INSTANCE)
		.setParameter("estacionamento", Objects.isNull(filtro.getEstacionamento()) ? null : filtro.getEstacionamento().getId(), IntegerType.INSTANCE)
		.setParameter("dataInicio", filtro.getDataInicio())
		.setParameter("dataFim", filtro.getDataFim());
		
		List<Object[]> list = q.getResultList();
		
		Map<Integer, BigInteger> map =  new HashMap<>();
		
		for(Object[] objects : list) {
			map.put((Integer)objects[0], (BigInteger)objects[1]);
		}	
		
		entityManager.close();
		return map;
	}
	
	public Map<String, BigInteger> getQuantidadeMediaEstacionamentoMes(Filtro filtro) {
		getInstance();
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query q = entityManager.createNativeQuery(
				"with total as (select \r\n" + 
				"		id, mes, valor, COUNT(id) OVER (PARTITION BY mes) as total\r\n" + 
				"	from (select\r\n" + 
				"		ROW_NUMBER() OVER (PARTITION BY to_char(data_entrada, 'MM') ORDER by count(he.id_historico_estacionamento)) as id,\r\n" + 
				"		to_char(data_entrada, 'MM') as mes,\r\n" + 
				"		to_char(data_entrada, 'YY') as ano,\r\n" + 
				"		count(he.id_historico_estacionamento) as valor\r\n" + 
				"	from public.historico_estacionamento as he \r\n" + 
				"	join public.vaga as v on v.id_vaga = he.id_vaga\r\n" + 
				"	join public.estacionamento as e on e.id_estacionamento = v.id_estacionamento\r\n" + 
				"	join public.instituicao as i on i.id_instituicao = e.id_instituicao\r\n" + 
				"	where \r\n" + 
				"		(1=1) \r\n" + 
				"		and (i.id_instituicao = :instituicao or :instituicao is null) \r\n" + 
				"		and (e.id_estacionamento = :estacionamento or :estacionamento is null) \r\n" + 
				"		and (he.data_entrada >= :dataInicio)\r\n" + 
				"		and (he.data_saida <= :dataFim) \r\n" +
				"	group by mes,ano\r\n" + 
				"	order by mes, ano, valor\r\n" + 
				"	) as query)\r\n" + 
				"	select mes, cast(avg(valor) as bigint) from total where id between (total + 1)/2 and (total + 2)/2 group by mes order by mes")
		.unwrap(NativeQuery.class)
		.setParameter("instituicao", Objects.isNull(filtro.getInstituicao()) ? null : filtro.getInstituicao().getId(), IntegerType.INSTANCE)
		.setParameter("estacionamento", Objects.isNull(filtro.getEstacionamento()) ? null : filtro.getEstacionamento().getId(), IntegerType.INSTANCE)
		.setParameter("dataInicio", filtro.getDataInicio())
		.setParameter("dataFim", filtro.getDataFim());
		
	
		
		List<Object[]> list = q.getResultList();
		
		Map<String, BigInteger> map =  new HashMap<>();
		
		for(Object[] objects : list) {
			map.put((String)objects[0], (BigInteger)objects[1]);
		}	
		
		entityManager.close();
		return map;
	}
	public Map<Integer, Integer> getQuantidadeEstacionamentoTotalMes(Filtro filtro) {
		getInstance();
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query q = entityManager.createNativeQuery(
				"	select\r\n" + 
				"		cast(to_char(data_entrada, 'MM') as int) as mes,\r\n" + 
				"		cast(count(he.id_historico_estacionamento) as int) as valor\r\n" + 
				"	from public.historico_estacionamento as he \r\n" + 
				"	join public.vaga as v on v.id_vaga = he.id_vaga\r\n" + 
				"	join public.estacionamento as e on e.id_estacionamento = v.id_estacionamento\r\n" + 
				"	join public.instituicao as i on i.id_instituicao = e.id_instituicao\r\n" + 
				"	where \r\n" + 
				"		(1=1) \r\n" + 
				"		and (i.id_instituicao = :instituicao or :instituicao is null) \r\n" + 
				"		and (e.id_estacionamento = :estacionamento or :estacionamento is null) \r\n" + 
				"		and (he.data_entrada >= :dataInicio)\r\n" + 
				"		and (he.data_saida <= :dataFim) \r\n" +
				"	group by mes\r\n" + 
				"	order by mes")
		.unwrap(NativeQuery.class)
		.setParameter("instituicao", Objects.isNull(filtro.getInstituicao()) ? null : filtro.getInstituicao().getId(), IntegerType.INSTANCE)
		.setParameter("estacionamento", Objects.isNull(filtro.getEstacionamento()) ? null : filtro.getEstacionamento().getId(), IntegerType.INSTANCE)
		.setParameter("dataInicio", filtro.getDataInicio())
		.setParameter("dataFim", filtro.getDataFim());
	
		List<Object[]> list = q.getResultList();
		
		Map<Integer, Integer> map =  new HashMap<>();
		
		for(Object[] objects : list) {
			map.put((Integer)objects[0], (Integer)objects[1]);
		}	
		
		entityManager.close();
		return map;
	}
	
	private static synchronized EntityManagerFactory getInstance() {
		if(Objects.isNull(entityManagerFactory)) {
			entityManagerFactory = Persistence.createEntityManagerFactory("smartparkinglocal");
		}
		return entityManagerFactory;
	}
}
