package br.com.ufrn.imd.smartparking.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import br.com.ufrn.imd.smartparking.dominio.VagaTipo;


@Named
@ViewScoped
public class VagaTipoBean implements Serializable{

	private static final long serialVersionUID = -7244366588970967058L;

	public List<VagaTipo> getCombo(){
		ArrayList<VagaTipo> list = new ArrayList<>();
    	list.add(VagaTipo.DEFICIENTE);
    	list.add(VagaTipo.IDOSO);
    	list.add(VagaTipo.PUBLICO);
    	list.add(VagaTipo.PRIVADO);
    	return list;
	}
}
