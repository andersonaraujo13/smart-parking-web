package br.com.ufrn.imd.smartparking.dominio;

public enum VinculoTipo {
	ADMINISTRADOR("ADMINISTRADOR"), MANTENEDOR("MANTENEDOR");
    
    private final String valor;
    
    VinculoTipo(String valorOpcao){
        valor = valorOpcao;
    }
    
    public String getValor(){
        return valor;
    }
}
