package br.com.ufrn.imd.smartparking.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import br.com.ufrn.imd.smartparking.dominio.Estacionamento;
import br.com.ufrn.imd.smartparking.dto.EstacionamentoDto;

public class EstacionamentoMapper {
	
	public static List<EstacionamentoDto> map(List<Estacionamento> estacionamentos){
		List<EstacionamentoDto> listDto = new ArrayList<>();
		for(Estacionamento estacionamento : estacionamentos) {
			listDto.add(new EstacionamentoDto(estacionamento.getId() ,estacionamento.getNome(), estacionamento.getLatitude(), estacionamento.getLongitude()));
		}
		
		return listDto;
	} 
}
