package br.com.ufrn.imd.smartparking.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import br.com.ufrn.imd.smartparking.dominio.MonitoramentoTipo;

@Named
@ViewScoped
public class MonitoramentoTipoBean implements Serializable {

	private static final long serialVersionUID = 645573261547033434L;

	public List<MonitoramentoTipo> getCombo(){
		List<MonitoramentoTipo> combo = new ArrayList<>();
		combo.add(MonitoramentoTipo.CAMERA);
		combo.add(MonitoramentoTipo.SENSOR_DISTANCIA);
		combo.add(MonitoramentoTipo.SENSOR_INFRAVERMELHO);
		combo.add(MonitoramentoTipo.SENSOR_PRESENCA);
		
		return combo;
	}
}
