package br.com.ufrn.imd.smartparking.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Specializes;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.util.Faces;

import com.github.adminfaces.template.session.AdminSession;

import br.com.ufrn.imd.smartparking.dominio.Estacionamento;
import br.com.ufrn.imd.smartparking.dominio.Usuario;
import br.com.ufrn.imd.smartparking.dominio.Vinculo;
import br.com.ufrn.imd.smartparking.service.EstacionamentoService;
import br.com.ufrn.imd.smartparking.service.UsuarioService;
import br.com.ufrn.imd.smartparking.utils.MessageUtils;
import br.com.ufrn.imd.smartparking.utils.Utils;


@Named
@SessionScoped
@Specializes
public class LogonMB extends AdminSession implements Serializable {

	private static final long serialVersionUID = -2936254469938196958L;
	@Inject
	private UsuarioService usuarioService;
	@Inject
	private EstacionamentoService estacionamentoService;
	private Usuario user;
	
    private String login;
    private String password;
    private boolean remember;


    public void login() throws IOException {
    	Optional<Usuario> autenticacao = usuarioService.login(login, password);
        if(autenticacao.isPresent()) {
        	user = autenticacao.get();
        	Vinculo vinculo = user.getInstituicoes().stream().findFirst().orElseGet(null);
        	
        	List<Estacionamento> list = estacionamentoService.findEstacionamentosByUser(user);
        	Estacionamento estacionamento = list.stream().findFirst().orElse(null);
        	
			MessageUtils.addMensagemInfo("Bem vindo ao sistema, " + user.getPessoa().getNome() + ".");
			Faces.getExternalContext().getFlash().setKeepMessages(true);
        	Utils.getSession().setAttribute("usuario", user);
        	Utils.getSession().setAttribute("vinculo", vinculo);
        	Utils.getSession().setAttribute("estacionamentoInicial", estacionamento);
        	
        	Faces.redirect("admin/dashboard/sintetico.jsf");
        }
    }

    @Override
    public boolean isLoggedIn() {
        return Objects.nonNull(user);
    }

    public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRemember() {
        return remember;
    }

    public void setRemember(boolean remember) {
        this.remember = remember;
    }

	public Usuario getUser() {
		return user;
	}
	
	public void doLogout() throws IOException {
		Faces.getSession().invalidate();
		user = null;
        Faces.redirect("public/login.jsf");		
	}
    
}
