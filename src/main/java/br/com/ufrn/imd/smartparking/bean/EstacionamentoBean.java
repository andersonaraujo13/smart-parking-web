package br.com.ufrn.imd.smartparking.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.ufrn.imd.smartparking.dominio.Endereco;
import br.com.ufrn.imd.smartparking.dominio.Estacionamento;
import br.com.ufrn.imd.smartparking.dominio.Usuario;
import br.com.ufrn.imd.smartparking.dominio.Vinculo;
import br.com.ufrn.imd.smartparking.service.EstacionamentoService;
import br.com.ufrn.imd.smartparking.utils.Utils;

@Named
@ViewScoped
public class EstacionamentoBean implements Serializable {
	
	private static final long serialVersionUID = 2574373534639645174L;
	@Inject
	private Estacionamento estacionamento;
	@Inject
	private Endereco endereco;
	@Inject
	private EstacionamentoService estacionamentoService;
	@Inject
	private VinculoBean vinculoBean;
	private Usuario user;
	
	private List<Estacionamento> listEstacionamentos;
	
	@PostConstruct
	public void init() {
		user = (Usuario) Utils.getSession().getAttribute("usuario");
		Estacionamento estacionamentoEdit = (Estacionamento) Utils.getSession().getAttribute("estacionamentoEdit");
		
		if(Objects.nonNull(estacionamentoEdit)) {
			estacionamento = estacionamentoEdit;
			endereco = estacionamentoEdit.getEndereco();
		}

		listEstacionamentos = estacionamentoService.findEstacionamentosByUser(user);
	}
	
	public void cadastrar() throws IOException {
		Vinculo vinculo = vinculoBean.getVinculo(estacionamento.getInstituicao());
		estacionamento.setEndereco(endereco);
		estacionamento.setAtivo(true);
		estacionamentoService.save(estacionamento, vinculo);
		Utils.getSession().setAttribute("estacionamentoEdit", null);
	}
	
	public void reset() {
		estacionamento = new Estacionamento();
		endereco = new Endereco();
	}
	
	public Estacionamento getEstacionamento() {
		return estacionamento;
	}
	public void setEstacionamento(Estacionamento estacionamento) {
		this.estacionamento = estacionamento;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Estacionamento> getListEstacionamentos() {
		return listEstacionamentos;
	}

	public void setListEstacionamentos(List<Estacionamento> listEstacionamentos) {
		this.listEstacionamentos = listEstacionamentos;
	}
	
	public String adicionarVaga(Estacionamento estacionamento) {
		Utils.getSession().setAttribute("estacionamento", estacionamento);
		return "/admin/vaga/cadastro.jsf?faces-redirect=true";
	}
	
	public String editar(Estacionamento estacionamento) {
		Utils.getSession().setAttribute("estacionamentoEdit", estacionamento);
		return "/admin/estacionamento/cadastro.jsf?faces-redirect=true";
	}
	
	public void remover(Estacionamento estacionamento) throws IOException {
		Vinculo vinculo = vinculoBean.getVinculo(estacionamento.getInstituicao());
		estacionamentoService.remove(estacionamento, vinculo);
		listEstacionamentos = estacionamentoService.findEstacionamentosByUser(user);
	}
	
}
