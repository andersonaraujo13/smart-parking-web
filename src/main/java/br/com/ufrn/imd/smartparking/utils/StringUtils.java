package br.com.ufrn.imd.smartparking.utils;

import java.security.MessageDigest;

public class StringUtils {
	
	public static String encriptMD5(String txt) {
        try {
            
                MessageDigest algorithm = MessageDigest.getInstance("MD5");
                byte messageDigest[] = algorithm.digest(txt.getBytes("UTF-8"));
                StringBuilder hexString = new StringBuilder();
                for (byte b : messageDigest) {
                  hexString.append(String.format("%02X", 0xFF & b));
                }
                String passMd5 = hexString.toString();
                //converter a string para minúsculo pois a função está gerando o md5 tudo maiusculo
                return passMd5.toLowerCase();
        } catch (Exception e) {
            return null;
        }

    }
}
