package br.com.ufrn.imd.smartparking.dominio;

public enum VagaTipo {
	IDOSO("IDOSO"), DEFICIENTE("DEFICIENTE"), PRIVADO("PRIVADO"), PUBLICO("PUBLICO");
    
    private final String valor;
    
    VagaTipo(String valorOpcao){
        valor = valorOpcao;
    }
    
    public String getValor(){
        return valor;
    }
}
