package br.com.ufrn.imd.smartparking.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.primefaces.PrimeFaces;

import br.com.ufrn.imd.smartparking.dao.VagaDao;
import br.com.ufrn.imd.smartparking.dominio.Estacionamento;
import br.com.ufrn.imd.smartparking.dominio.Vaga;
import br.com.ufrn.imd.smartparking.utils.MessageUtils;

@Stateless
public class VagaService implements Serializable{

	private static final long serialVersionUID = 4437116088095633109L;
	
	@Inject
	private VagaDao vagaDao;

	public List<Vaga> findByEstacionamento(Estacionamento estacionamento) {
		List<Vaga> listVagas = vagaDao.findByEstacionamento(estacionamento).orElse(new ArrayList<Vaga>());
		return listVagas;
	}
	
	public List<Vaga> findByEstacionamentoID(Integer id) {
		List<Vaga> listVagas = vagaDao.findByEstacionamentoId(id).orElse(new ArrayList<Vaga>());
		return listVagas;
	}
	
	public void cadastrar(Vaga vaga) {
		try {
			vagaDao.cadastrar(vaga);
			PrimeFaces.current().executeScript("PF('modal-cadastro').hide();");
			MessageUtils.addMensagemInfo("Vaga Cadastrada com Sucesso.");
		} catch (Exception e) {
			PrimeFaces.current().executeScript("PF('modal-cadastro').hide();");
			MessageUtils.addMensagemWarn("Não foi possível realizar o cadastro da vaga.");
		}
	}

	public void remover(Vaga vaga) {
		vagaDao.remover(vaga);
	}

}
