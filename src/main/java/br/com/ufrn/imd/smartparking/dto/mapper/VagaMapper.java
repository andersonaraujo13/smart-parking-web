package br.com.ufrn.imd.smartparking.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import br.com.ufrn.imd.smartparking.dominio.Vaga;
import br.com.ufrn.imd.smartparking.dominio.VagaTipo;
import br.com.ufrn.imd.smartparking.dto.VagaDto;

public class VagaMapper {
	
	
	public static List<VagaDto> map(List<Vaga> vagas){
		List<VagaDto> vagasDto = new ArrayList<>();
		for(Vaga vaga : vagas) {
			VagaDto dto = new VagaDto(vaga.getId(), vaga.getApelido(), vaga.getLatitude(), vaga.getLongitude());
			
			if(VagaTipo.DEFICIENTE == vaga.getTipo()) {
				dto.setCss(getIcon("#2f7df9"));
			} else if(VagaTipo.IDOSO == vaga.getTipo()) {
				dto.setCss(getIcon("#db93ff"));
			} else if(VagaTipo.PRIVADO == vaga.getTipo()) {
				dto.setCss(getIcon("#f9f92f"));
			} else if(VagaTipo.PUBLICO == vaga.getTipo()) {
				dto.setCss(getIcon("#3ff92f"));
			}
			
			if(vaga.isOcupado()) {
				dto.setCss(getIcon("#fc2828"));
			}
			
			vagasDto.add(dto);
		}
		
		return vagasDto;
	}
	
	public static String getIcon(String color) {
		String icon = "<span style=\""+
				"background-color:" +  color + ";" +
				"width: 1rem;" + 
				"height: 1rem;" + 
				"display: block;" + 
				"left: -1.5rem;" + 
				"top: -1.5rem;" + 
				"position: relative;" + 
				"transform: rotate(45deg);" + 
				"border: 1px solid #FFFFFF;\" />";
		return icon;
	}
	
}
