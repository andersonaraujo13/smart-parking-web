package br.com.ufrn.imd.smartparking.bean;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.ufrn.imd.smartparking.dominio.Estacionamento;
import br.com.ufrn.imd.smartparking.dominio.Vaga;
import br.com.ufrn.imd.smartparking.service.VagaService;
import br.com.ufrn.imd.smartparking.utils.Utils;

@Named
@ViewScoped
public class VagaBean implements Serializable {

	private static final long serialVersionUID = 3175129627086970649L;
	
	@Inject
	private VagaService vagaService;
	
	private Estacionamento estacionamento;
	private List<Vaga> listVagas;
	private Vaga vagaCadastro;
	
	
	@PostConstruct
	public void init() {
		estacionamento = (Estacionamento) Utils.getSession().getAttribute("estacionamento");
		listVagas = vagaService.findByEstacionamento(estacionamento);
		listVagas = ordenar(listVagas);
		vagaCadastro = new Vaga();
	}
	
	public List<Vaga> getListVagas() {
		return listVagas;
	}
	
	public Estacionamento getEstacionamento() {
		return estacionamento;
	}

	public Vaga getVagaCadastro() {
		return vagaCadastro;
	}

	public void setVagaCadastro(Vaga vagaCadastro) {
		this.vagaCadastro = vagaCadastro;
	}
	
	public void cadastrar() {
		vagaCadastro.setEstacionamento(estacionamento);
		vagaService.cadastrar(vagaCadastro);
		reset();
	}
	
	public void reset(){
		vagaCadastro = new Vaga();
		listVagas = vagaService.findByEstacionamento(estacionamento);
		listVagas = ordenar(listVagas);
	}
	
	public void editar(Vaga vaga) {
		this.vagaCadastro = vaga;
	}
	
	public void remover(Vaga vaga) {
		vagaService.remover(vaga);
		reset();
	}
	
	private List<Vaga> ordenar(List<Vaga> vagas){
		return vagas.stream().sorted((v1,v2) -> v1.getApelido().compareTo(v2.getApelido())).collect(Collectors.toList());
	}
}
