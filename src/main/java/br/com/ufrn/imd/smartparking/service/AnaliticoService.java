package br.com.ufrn.imd.smartparking.service;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.primefaces.component.dashboard.Dashboard;

import br.com.ufrn.imd.smartparking.dao.AnaliticoDao;
import br.com.ufrn.imd.smartparking.dominio.Estacionamento;
import br.com.ufrn.imd.smartparking.dominio.Filtro;
import br.com.ufrn.imd.smartparking.dominio.Instituicao;

@Stateless
public class AnaliticoService implements Serializable {
	
	private static final long serialVersionUID = -2043522955909131663L;
	@Inject
	private AnaliticoDao analiticoDao;
	
	public Integer getTotalEstacionamento(Filtro filtro) {
		return analiticoDao.getTotalEstacionamento(filtro);
	}
	public Integer getTotalTempo(Filtro filtro) {
		return analiticoDao.getTotalTempo(filtro);
	}
	public Map<Integer, BigInteger> getTempoMedioSemana(Filtro filtro) {
		return analiticoDao.getTempoMedioSemana(filtro);
	}
	public Map<Integer, BigInteger> getEstacionamentoMedioSemana(Filtro filtro) {
		return analiticoDao.getQuantidadeMediaEstacionamentoSemana(filtro);
	}
	public Map<Integer, Integer> getEstacionamentoTotalSemana(Filtro filtro) {
		return analiticoDao.getQuantidadeEstacionamentoTotalSemana(filtro);
	}
	public Map<Integer, Integer> getEstacionamentoTotalMes(Filtro filtro) {
		return analiticoDao.getQuantidadeEstacionamentoTotalMes(filtro);
	}
	public Map<Integer, BigInteger> getEstacionamentoTempoMedioMes(Filtro filtro) {
		return analiticoDao.getTempoMedioMes(filtro);
	}
}
