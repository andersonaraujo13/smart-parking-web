package br.com.ufrn.imd.smartparking.dominio;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vaga", schema = "public")
public class Vaga implements Serializable{

	private static final long serialVersionUID = -1201328542901008561L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_vaga", unique = true, nullable = false)
	private int id;
	
	@Column(name = "apelido")
	private String apelido; 
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_estacionamento")
	private Estacionamento estacionamento;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_vaga")
	private VagaTipo tipo;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_monitoramento")
	private MonitoramentoTipo monitoramentoTipo;
	
	@Column(name = "latitude")
	private Double latitude;
	
	@Column(name = "longitude")
	private Double longitude;	
	
	@Column(name = "ocupado")
	private boolean ocupado;
	
	@Column(name = "autenticado")
	private boolean autenticado;
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getApelido() {
		return apelido;
	}
	public void setApelido(String apelido) {
		this.apelido = apelido;
	}
	public VagaTipo getTipo() {
		return tipo;
	}
	public void setTipo(VagaTipo tipo) {
		this.tipo = tipo;
	}
	public Estacionamento getEstacionamento() {
		return estacionamento;
	}
	public void setEstacionamento(Estacionamento estacionamento) {
		this.estacionamento = estacionamento;
	}
	public boolean isOcupado() {
		return ocupado;
	}
	public void setOcupado(boolean ocupado) {
		this.ocupado = ocupado;
	}
	public MonitoramentoTipo getMonitoramentoTipo() {
		return monitoramentoTipo;
	}
	public void setMonitoramentoTipo(MonitoramentoTipo monitoramentoTipo) {
		this.monitoramentoTipo = monitoramentoTipo;
	}
	public boolean isAutenticado() {
		return autenticado;
	}
	public void setAutenticado(boolean autenticado) {
		this.autenticado = autenticado;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}