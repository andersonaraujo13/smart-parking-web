package br.com.ufrn.imd.smartparking.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.axes.cartesian.CartesianScales;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearAxes;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearTicks;
import org.primefaces.model.charts.bar.BarChartDataSet;
import org.primefaces.model.charts.bar.BarChartModel;
import org.primefaces.model.charts.bar.BarChartOptions;
import org.primefaces.model.charts.line.LineChartDataSet;
import org.primefaces.model.charts.line.LineChartModel;
import org.primefaces.model.charts.line.LineChartOptions;
import org.primefaces.model.charts.optionconfig.legend.Legend;
import org.primefaces.model.charts.optionconfig.legend.LegendLabel;
import org.primefaces.model.charts.optionconfig.title.Title;
import org.primefaces.model.charts.pie.PieChartDataSet;
import org.primefaces.model.charts.pie.PieChartModel;

import br.com.ufrn.imd.smartparking.dominio.Estacionamento;
import br.com.ufrn.imd.smartparking.dominio.Filtro;
import br.com.ufrn.imd.smartparking.service.AnaliticoService;
import br.com.ufrn.imd.smartparking.utils.DashboardUtils;
import br.com.ufrn.imd.smartparking.utils.Utils;

@Named
@ViewScoped
public class AnaliticoBean implements Serializable {

	private static final long serialVersionUID = 3592402886275277288L;
	
	@Inject
	private AnaliticoService analiticoService;
	private Map<String, Integer> card;
	private Map<String, Integer> totalOcupados;
	private Map<String, Integer> totalSensoriamento;
	private Map<Integer, String> meses;
	private Map<Integer, String> dias;
	private Map<Integer, String> colors;
	private Map<Integer, String> borders; 
	
	private Integer totalEstacionamento;
	private Integer totalTempo;
	private PieChartModel pieSensoriamento;
	private LineChartModel lineTempoMedioSemana;
	private LineChartModel lineEstacionamentoMedioSemana;
	private BarChartModel barEstacionamentoTotalSemana;
	private LineChartModel lineTempoMedioMes;
	private BarChartModel barEstacionamentoTotalMes;
	private Filtro filtro;
	
	@PostConstruct
	public void init() {
		//card = analiticoService.totalVagas(null, null);
		//totalOcupados = analiticoService.totalVagasOcupadas(null, null);
		//totalSensoriamento = analiticoService.getSensoriamento(null, null);
		
		//loadSensoriamento();
		Estacionamento estacionamento = (Estacionamento) Utils.getSession().getAttribute("estacionamentoInicial");
		LocalDate now = LocalDate.now(); 
		LocalDate firstDay = now.with(TemporalAdjusters.firstDayOfYear()); 
		LocalDate lastDay = now.with(TemporalAdjusters.lastDayOfYear());
		filtro = new Filtro();
		filtro.setDataInicio(Date.from(firstDay.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		filtro.setDataFim(Date.from(lastDay.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		filtro.setEstacionamento(estacionamento);
		filtro.setInstituicao(estacionamento.getInstituicao());
		
		meses = DashboardUtils.getMeses();
		dias = DashboardUtils.getDias();
		colors = DashboardUtils.getColors();
        borders = DashboardUtils.getBorders();
        
		totalEstacionamento = analiticoService.getTotalEstacionamento(filtro);
		totalTempo = analiticoService.getTotalTempo(filtro);
		loadTempoMedioSemana();
		loadEstacionamentoMedioSemana();
		loadEstacionamentoTotalSemana();
		loadEstacionamentoTempoMedioMes();
		loadEstacionamentoTotalMes();
	} 
	
	private void loadEstacionamentoTotalMes() {
		barEstacionamentoTotalMes = new BarChartModel();
        ChartData data = new ChartData();
         
        BarChartDataSet barDataSet = new BarChartDataSet();
        barDataSet.setLabel("My First Dataset");
         
        Map<Integer, Integer> quantidadeTotalSemana = analiticoService.getEstacionamentoTotalMes(filtro);
        
        List<String> bgColor = new ArrayList<>();
        List<String> borderColor = new ArrayList<>();
         
        List<Number> values = new ArrayList<>();
        List<String> labels = new ArrayList<>();
        
        for(Integer key : quantidadeTotalSemana.keySet()) {
        	values.add(quantidadeTotalSemana.getOrDefault(key, 0));
        	labels.add(meses.get(key));
        	bgColor.add(colors.get(key));
        	borderColor.add(borders.get(key));
        }
        
        barDataSet.setBackgroundColor(bgColor);
        barDataSet.setBorderColor(borderColor);
        barDataSet.setBorderWidth(1);
         
        data.addChartDataSet(barDataSet);
        
        barEstacionamentoTotalMes.setData(data);
         
        BarChartOptions options = new BarChartOptions();
        CartesianScales cScales = new CartesianScales();
        CartesianLinearAxes linearAxes = new CartesianLinearAxes();
        CartesianLinearTicks ticks = new CartesianLinearTicks();
        ticks.setBeginAtZero(true);
        linearAxes.setTicks(ticks);
        cScales.addYAxesData(linearAxes);
        options.setScales(cScales);
         
        Title title = new Title();
        title.setDisplay(false);
        title.setText("Bar Chart");
        options.setTitle(title);
 
        Legend legend = new Legend();
        legend.setDisplay(false);
        legend.setPosition("top");
        
        LegendLabel legendLabels = new LegendLabel();
        legendLabels.setFontStyle("bold");
        legendLabels.setFontColor("#2980B9");
        legendLabels.setFontSize(24);
        
        legend.setLabels(legendLabels);
        
        options.setLegend(legend);
 
        data.setLabels(labels);
        barDataSet.setData(values);
        barEstacionamentoTotalMes.setOptions(options);
		
	}

	private void loadEstacionamentoTempoMedioMes() {
		lineTempoMedioMes = new LineChartModel();
        ChartData data = new ChartData();
         
        LineChartDataSet dataSet = new LineChartDataSet();
        
        Map<Integer, BigInteger> quantidadeSemana = analiticoService.getEstacionamentoTempoMedioMes(filtro);
       
        dataSet.setFill(false);
        dataSet.setLabel("2019");
        dataSet.setBorderColor("rgb(69, 3, 252)");
        dataSet.setLineTension(0.1);
        data.addChartDataSet(dataSet);
       
        List<Number> values = new ArrayList<>();
        List<String> labels = new ArrayList<>();
       
     
        for(Integer key : quantidadeSemana.keySet()) {
        	values.add(quantidadeSemana.getOrDefault(key, BigInteger.ZERO));
        	labels.add(meses.get(key));
        }
        
        LineChartOptions options = new LineChartOptions();
        CartesianScales cScales = new CartesianScales();
        CartesianLinearAxes linearAxes = new CartesianLinearAxes();
        CartesianLinearTicks ticks = new CartesianLinearTicks();
        ticks.setBeginAtZero(true);
        linearAxes.setTicks(ticks);
        cScales.addYAxesData(linearAxes);
        options.setScales(cScales);
         
        Title title = new Title();
        title.setDisplay(false);
        title.setText("Bar Chart");
        options.setTitle(title);
 
        Legend legend = new Legend();
        legend.setDisplay(false);
        legend.setPosition("top");
        
        LegendLabel legendLabels = new LegendLabel();
        legendLabels.setFontStyle("bold");
        legendLabels.setFontColor("#2980B9");
        legendLabels.setFontSize(24);
        
        legend.setLabels(legendLabels);
        
        options.setLegend(legend);
       
        data.setLabels(labels);
        dataSet.setData(values);
        lineTempoMedioMes.setData(data);
        lineTempoMedioMes.setOptions(options);
		
	}

	private void loadEstacionamentoTotalSemana() {
		barEstacionamentoTotalSemana = new BarChartModel();
        ChartData data = new ChartData();
         
        BarChartDataSet barDataSet = new BarChartDataSet();
        barDataSet.setLabel("My First Dataset");
         
        Map<Integer, Integer> quantidadeTotalSemana = analiticoService.getEstacionamentoTotalSemana(filtro);
         
        List<String> bgColor = new ArrayList<>();
        List<String> borderColor = new ArrayList<>();
         
        List<Number> values = new ArrayList<>();
        List<String> labels = new ArrayList<>();
        
        for(Integer key : quantidadeTotalSemana.keySet()) {
        	values.add(quantidadeTotalSemana.getOrDefault(key,0));
        	labels.add(dias.get(key));
        	bgColor.add(colors.get(key));
        	borderColor.add(borders.get(key));
        }
        
        barDataSet.setBackgroundColor(bgColor);
        barDataSet.setBorderColor(borderColor);
        barDataSet.setBorderWidth(1);
         
        data.addChartDataSet(barDataSet);
        barEstacionamentoTotalSemana.setData(data);
         
        BarChartOptions options = new BarChartOptions();
        CartesianScales cScales = new CartesianScales();
        CartesianLinearAxes linearAxes = new CartesianLinearAxes();
        CartesianLinearTicks ticks = new CartesianLinearTicks();
        ticks.setBeginAtZero(true);
        linearAxes.setTicks(ticks);
        cScales.addYAxesData(linearAxes);
        options.setScales(cScales);
         
        Title title = new Title();
        title.setDisplay(false);
        title.setText("Bar Chart");
        options.setTitle(title);
 
        Legend legend = new Legend();
        legend.setDisplay(false);
        legend.setPosition("top");
        
        LegendLabel legendLabels = new LegendLabel();
        legendLabels.setFontStyle("bold");
        legendLabels.setFontColor("#2980B9");
        legendLabels.setFontSize(24);
        
        legend.setLabels(legendLabels);
        
        options.setLegend(legend);
 
        data.setLabels(labels);
        barDataSet.setData(values);
        barEstacionamentoTotalSemana.setOptions(options);
    }
	
	private void loadEstacionamentoMedioSemana() {
		lineEstacionamentoMedioSemana = new LineChartModel();
        ChartData data = new ChartData();
         
        LineChartDataSet dataSet = new LineChartDataSet();
        
        Map<Integer, BigInteger> quantidadeSemana = analiticoService.getEstacionamentoMedioSemana(filtro);
       
        dataSet.setFill(false);
        dataSet.setLabel("2019");
        dataSet.setBorderColor("rgb(69, 3, 252)");
        dataSet.setLineTension(0.1);
        data.addChartDataSet(dataSet);
       
        List<Number> values = new ArrayList<>();
        List<String> labels = new ArrayList<>();
       
     
        for(Integer key : quantidadeSemana.keySet()) {
        	values.add(quantidadeSemana.getOrDefault(key, BigInteger.ZERO));
        	labels.add(dias.get(key));
        }
        
        LineChartOptions options = new LineChartOptions();
        CartesianScales cScales = new CartesianScales();
        CartesianLinearAxes linearAxes = new CartesianLinearAxes();
        CartesianLinearTicks ticks = new CartesianLinearTicks();
        ticks.setBeginAtZero(true);
        linearAxes.setTicks(ticks);
        cScales.addYAxesData(linearAxes);
        options.setScales(cScales);
         
        Title title = new Title();
        title.setDisplay(false);
        title.setText("Bar Chart");
        options.setTitle(title);
 
        Legend legend = new Legend();
        legend.setDisplay(false);
        legend.setPosition("top");
        
        LegendLabel legendLabels = new LegendLabel();
        legendLabels.setFontStyle("bold");
        legendLabels.setFontColor("#2980B9");
        legendLabels.setFontSize(24);
        
        legend.setLabels(legendLabels);
        
        options.setLegend(legend);
       
        data.setLabels(labels);
        dataSet.setData(values);
        lineEstacionamentoMedioSemana.setData(data);
        lineEstacionamentoMedioSemana.setOptions(options);
		
	}

	private void loadTempoMedioSemana() {
		lineTempoMedioSemana = new LineChartModel();
        ChartData data = new ChartData();
         
        LineChartDataSet dataSet = new LineChartDataSet();
        
        Map<Integer, BigInteger> tempoSemana = analiticoService.getTempoMedioSemana(filtro);
       
        dataSet.setFill(false);
        dataSet.setLabel("2019");
        dataSet.setBorderColor("rgb(69, 3, 252)");
        dataSet.setLineTension(0.1);
        data.addChartDataSet(dataSet);
       
        List<Number> values = new ArrayList<>();
        List<String> labels = new ArrayList<>();
       
     
        for(Integer key : tempoSemana.keySet()) {
        	values.add(tempoSemana.getOrDefault(key, BigInteger.ZERO));
        	labels.add(dias.get(key));
        }
        
        LineChartOptions options = new LineChartOptions();
        CartesianScales cScales = new CartesianScales();
        CartesianLinearAxes linearAxes = new CartesianLinearAxes();
        CartesianLinearTicks ticks = new CartesianLinearTicks();
        ticks.setBeginAtZero(true);
        linearAxes.setTicks(ticks);
        cScales.addYAxesData(linearAxes);
        options.setScales(cScales);
         
        Title title = new Title();
        title.setDisplay(false);
        title.setText("Bar Chart");
        options.setTitle(title);
 
        Legend legend = new Legend();
        legend.setDisplay(false);
        legend.setPosition("top");
        
        LegendLabel legendLabels = new LegendLabel();
        legendLabels.setFontStyle("bold");
        legendLabels.setFontColor("#2980B9");
        legendLabels.setFontSize(24);
        
        legend.setLabels(legendLabels);
        options.setLegend(legend);
       
        data.setLabels(labels);
        dataSet.setData(values);
        lineTempoMedioSemana.setData(data);
        lineTempoMedioSemana.setOptions(options);
	}

	@Deprecated
	private void loadSensoriamento() {
		pieSensoriamento = new PieChartModel();
        ChartData data = new ChartData();
         
        PieChartDataSet dataSet = new PieChartDataSet();
         
        List<String> bgColors = new ArrayList<>();
        bgColors.add("rgb(255, 99, 132)");
        bgColors.add("rgb(54, 162, 235)");
        bgColors.add("rgb(255, 205, 86)");
        bgColors.add("rgb(255, 105, 86)");
        
        List<Number> values = new ArrayList<>();
        List<String> labels = new ArrayList<>();
        
        for(String key : totalSensoriamento.keySet()) {
        	values.add(totalSensoriamento.get(key));
        	labels.add(key);
        }
        dataSet.setBackgroundColor(bgColors);
        dataSet.setData(values);
        data.addChartDataSet(dataSet);
        data.setLabels(labels);
         
        pieSensoriamento.setData(data);
		
	}

	public Map<String, Integer> getCard() {
		return card;
	}
	
	public void setCard(Map<String, Integer> card) {
		this.card = card;
	}
	
	public Integer numeroVagas(String tipo) {
		Integer value = card.get(tipo);
		return Objects.isNull(value) ? 0 : value;
	}
	
	public Integer porcentagemOcupacao(String tipo) {
		Integer total = card.getOrDefault(tipo, 0);
		Integer ocupados = totalOcupados.getOrDefault(tipo, 0);
		
		if(total == 0) {
			return total;
		}
		
		return (ocupados * 100) / total;
	}

	public PieChartModel getPieSensoriamento() {
		return pieSensoriamento;
	}

	public void setPieSensoriamento(PieChartModel pieSensoriamento) {
		this.pieSensoriamento = pieSensoriamento;
	}

	public LineChartModel getLineTempoMedioSemana() {
		return lineTempoMedioSemana;
	}

	public void setLineTempoMedioSemana(LineChartModel lineTempoMedioSemana) {
		this.lineTempoMedioSemana = lineTempoMedioSemana;
	}

	public LineChartModel getLineEstacionamentoMedioSemana() {
		return lineEstacionamentoMedioSemana;
	}

	public void setLineEstacionamentoMedioSemana(LineChartModel lineEstacionamentoMedioSemana) {
		this.lineEstacionamentoMedioSemana = lineEstacionamentoMedioSemana;
	}

	public BarChartModel getBarEstacionamentoTotalSemana() {
		return barEstacionamentoTotalSemana;
	}

	public void setBarEstacionamentoTotalSemana(BarChartModel barEstacionamentoTotalSemana) {
		this.barEstacionamentoTotalSemana = barEstacionamentoTotalSemana;
	}

	public LineChartModel getLineTempoMedioMes() {
		return lineTempoMedioMes;
	}

	public void setLineTempoMedioMes(LineChartModel lineTempoMedioMes) {
		this.lineTempoMedioMes = lineTempoMedioMes;
	}

	public BarChartModel getBarEstacionamentoTotalMes() {
		return barEstacionamentoTotalMes;
	}

	public void setBarEstacionamentoTotalMes(BarChartModel barEstacionamentoTotalMes) {
		this.barEstacionamentoTotalMes = barEstacionamentoTotalMes;
	}

	public Map<String, Integer> getTotalSensoriamento() {
		return totalSensoriamento;
	}

	public void setTotalSensoriamento(Map<String, Integer> totalSensoriamento) {
		this.totalSensoriamento = totalSensoriamento;
	}

	public Integer getTotalTempo() {
		return totalTempo;
	}

	public void setTotalTempo(Integer totalTempo) {
		this.totalTempo = totalTempo;
	}

	public Integer getTotalEstacionamento() {
		return totalEstacionamento;
	}

	public void setTotalEstacionamento(Integer totalEstacionamento) {
		this.totalEstacionamento = totalEstacionamento;
	}
}