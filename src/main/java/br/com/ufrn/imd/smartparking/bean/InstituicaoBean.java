package br.com.ufrn.imd.smartparking.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import br.com.ufrn.imd.smartparking.dominio.Instituicao;
import br.com.ufrn.imd.smartparking.dominio.Usuario;
import br.com.ufrn.imd.smartparking.utils.Utils;

@Named
@ViewScoped
public class InstituicaoBean implements Serializable{

	private static final long serialVersionUID = -1841239853372774446L;
	
	public List<Instituicao> getCombo(){
		Usuario usuario = (Usuario) Utils.getSession().getAttribute("usuario");
		List<Instituicao> instituicoes = new ArrayList<>();
		
		if(Objects.nonNull(usuario.getInstituicoes()) && !usuario.getInstituicoes().isEmpty()) {
			usuario.getInstituicoes().stream().forEach(entity ->{
				instituicoes.add(entity.getInstituicao());
			});
		}
		return instituicoes;
	}
}
