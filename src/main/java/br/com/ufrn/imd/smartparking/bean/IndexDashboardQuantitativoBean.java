package br.com.ufrn.imd.smartparking.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.line.LineChartDataSet;
import org.primefaces.model.charts.line.LineChartModel;
import org.primefaces.model.charts.pie.PieChartDataSet;
import org.primefaces.model.charts.pie.PieChartModel;

@Named
@ViewScoped
public class IndexDashboardQuantitativoBean implements Serializable {
	private static final long serialVersionUID = 4340272769131558310L;
	private PieChartModel pieModel;
	private LineChartModel lineModel;
	private LineChartModel lineModel2;
	
	@PostConstruct
	public void init() {
		createPieModel();
		createLineModel();
		createLineModel2();
	}
	
	private void createPieModel() {
        pieModel = new PieChartModel();
        ChartData data = new ChartData();
         
        PieChartDataSet dataSet = new PieChartDataSet();
        List<Number> values = new ArrayList<>();
        values.add(300);
        values.add(50);
        values.add(100);
        values.add(344);
        dataSet.setData(values);
         
        List<String> bgColors = new ArrayList<>();
        bgColors.add("rgb(255, 99, 132)");
        bgColors.add("rgb(54, 162, 235)");
        bgColors.add("rgb(255, 205, 86)");
        bgColors.add("rgb(255, 105, 86)");
        dataSet.setBackgroundColor(bgColors);
         
        data.addChartDataSet(dataSet);
        List<String> labels = new ArrayList<>();
        labels.add("Câmera");
        labels.add("Sensor de Distância");
        labels.add("Sensor de Presença");
        labels.add("Sensor de Infra-Vermelho");
        data.setLabels(labels);
         
        pieModel.setData(data);
    }
	
	public void createLineModel() {
        lineModel = new LineChartModel();
        ChartData data = new ChartData();
         
        LineChartDataSet dataSet = new LineChartDataSet();
        List<Number> values = new ArrayList<>();
        values.add(120);
        values.add(110);
        values.add(90);
        values.add(100);
        values.add(70);
        values.add(30);
        values.add(50);
        dataSet.setData(values);
        dataSet.setFill(false);
        dataSet.setLabel("2019");
        dataSet.setBorderColor("rgb(75, 192, 192)");
        dataSet.setLineTension(0.1);
        data.addChartDataSet(dataSet);
         
        List<String> labels = new ArrayList<>();
        labels.add("Segunda");
        labels.add("Terça");
        labels.add("Quarta");
        labels.add("Quinta");
        labels.add("Sexta");
        labels.add("Sábado");
        labels.add("Domingo");
        data.setLabels(labels);
         
        lineModel.setData(data);
        
    }
	public void createLineModel2() {
        lineModel2 = new LineChartModel();
        ChartData data = new ChartData();
         
        LineChartDataSet dataSet = new LineChartDataSet();
        List<Number> values = new ArrayList<>();
        values.add(65);
        values.add(59);
        values.add(80);
        values.add(81);
        values.add(56);
        values.add(55);
        values.add(40);
        dataSet.setData(values);
        dataSet.setFill(false);
        dataSet.setLabel("2019");
        dataSet.setBorderColor("rgb(175, 192, 192)");
        dataSet.setLineTension(0.1);
        data.addChartDataSet(dataSet);
         
        List<String> labels = new ArrayList<>();
        labels.add("Segunda");
        labels.add("Terça");
        labels.add("Quarta");
        labels.add("Quinta");
        labels.add("Sexta");
        labels.add("Sábado");
        labels.add("Domingo");
        data.setLabels(labels);
         
        lineModel2.setData(data);
        
    } 

	public PieChartModel getPieModel() {
		return pieModel;
	}

	public void setPieModel(PieChartModel pieModel) {
		this.pieModel = pieModel;
	}

	public LineChartModel getLineModel() {
		return lineModel;
	}

	public void setLineModel(LineChartModel lineModel) {
		this.lineModel = lineModel;
	}

	public LineChartModel getLineModel2() {
		return lineModel2;
	}

	public void setLineModel2(LineChartModel lineModel2) {
		this.lineModel2 = lineModel2;
	}
	
}