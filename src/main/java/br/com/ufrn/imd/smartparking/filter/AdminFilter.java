package br.com.ufrn.imd.smartparking.filter;

import java.io.IOException;
import java.util.Objects;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import br.com.ufrn.imd.smartparking.bean.LogonMB;

public class AdminFilter implements Filter {
	
	@Inject
	private LogonMB logon;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletResponse res = (HttpServletResponse) response;
		
		if (logon.isLoggedIn() && Objects.nonNull(logon.getUser())) {
			chain.doFilter(request, response);
		} else {
			res.sendRedirect("/smartparking/public/login.jsf");
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
