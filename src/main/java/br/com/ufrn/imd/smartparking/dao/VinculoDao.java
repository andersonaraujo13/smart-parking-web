package br.com.ufrn.imd.smartparking.dao;

import java.util.List;
import java.util.Optional;

import br.com.ufrn.imd.smartparking.dominio.Usuario;
import br.com.ufrn.imd.smartparking.dominio.Vinculo;
import br.com.ufrn.imd.smartparking.dominio.VinculoTipo;

public class VinculoDao extends GenericDao<Vinculo>{

	public Optional<List<Vinculo>> findVinculoByUserAndTipo(Usuario user, VinculoTipo tipo) {
		prepareBuildQuery();
		q.select(root);
		q.where(
				cb.and(
						cb.equal(root.get("usuario"), user), cb.equal(root.get("vinculo"), tipo)		
						)
				);
		
		return Optional.of(getInstance().createQuery(q).getResultList());
	}

}
