package br.com.ufrn.imd.smartparking.utils;

import java.util.HashMap;
import java.util.Map;

public class DashboardUtils {
	
	public static Map<Integer, String> getMeses() {
		Map<Integer, String> meses = new HashMap<>();
		meses.put(1, "Janeiro");
		meses.put(2, "Fevereiro");
		meses.put(3, "Março");
		meses.put(4, "Abril");
		meses.put(5, "Maio");
		meses.put(6, "Junho");
		meses.put(7, "Julho");
		meses.put(8, "Agosto");
		meses.put(9, "Setembro");
		meses.put(10, "Outubro");
		meses.put(11, "Novembro");
		meses.put(12, "Dezembro");
		return meses;
	}
	
	public static Map<Integer, String> getDias(){
		Map<Integer, String> dias = new HashMap<>();
		dias.put(1, "Domingo");
		dias.put(2, "Segunda");
		dias.put(3, "Terça");
		dias.put(4, "Quarta");
		dias.put(5, "Quinta");
		dias.put(6, "Sexta");
		dias.put(7, "Sabado");
		return dias;
	}
	
	public static Map<Integer, String> getColors(){
		Map<Integer, String> colors = new HashMap<>();
		colors.put(1, "rgba(245, 155, 66, 0.2)");
		colors.put(2, "rgba(245, 242, 66, 0.2)");
		colors.put(3, "rgba(182, 245, 66, 0.2)");
		colors.put(4, "rgba(120, 245, 66, 0.2)");
		colors.put(5, "rgba(66, 245, 75, 0.2)");
		colors.put(6, "rgba(66, 245, 138, 0.2)");
		colors.put(7, "rgba(66, 245, 209, 0.2)");
		colors.put(8, "rgba(66, 188, 245, 0.2)");
		colors.put(9, "rgba(66, 105, 245, 0.2)");
		colors.put(10, "rgba(132, 66, 245, 0.2)");
		colors.put(11, "rgba(255, 159, 64, 0.2)");
		colors.put(12, "rgba(245, 66, 173, 0.2)");
		return colors;
	}
	
	public static Map<Integer, String> getBorders(){
		Map<Integer, String> borders = new HashMap<>();
		borders.put(1,"rgb(255, 99, 132)");
		borders.put(2,"rgb(255, 159, 64)");
		borders.put(3,"rgb(255, 205, 86)");
		borders.put(4,"rgb(75, 192, 192)");
		borders.put(5,"rgb(54, 162, 235)");
	   	borders.put(6,"rgb(153, 102, 255)");
	   	borders.put(7,"rgb(201, 203, 207)");
	   	borders.put(8,"rgb(255, 205, 122)");
	   	borders.put(9,"rgb(75, 192, 231)");
        borders.put(10,"rgb(54, 162, 121)");
        borders.put(11,"rgb(153, 102, 111)");
        borders.put(12,"rgb(201, 203, 222)");
		return borders;
	}
}
