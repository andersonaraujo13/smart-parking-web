package br.com.ufrn.imd.smartparking.dao;

import java.util.List;
import java.util.Optional;

import br.com.ufrn.imd.smartparking.dominio.Estacionamento;
import br.com.ufrn.imd.smartparking.dominio.Vaga;
import br.com.ufrn.imd.smartparking.utils.MessageUtils;

public class VagaDao extends GenericDao<Vaga>{

	public  Optional<List<Vaga>> findByEstacionamento(Estacionamento estacionamento) {
		prepareBuildQuery();
		q.select(root);
		q.where(cb.equal(root.get("estacionamento"), estacionamento));
		
		return Optional.of(getInstance().createQuery(q).getResultList());
	}
	
	public void cadastrar(Vaga vaga) throws Exception {
		try {
			getInstance().getTransaction().begin();
				getInstance().persist(vaga);
			getInstance().getTransaction().commit();
		} catch (Exception e) {
			getInstance().getTransaction().rollback();
			throw new Exception(e);
		} 
	}

	public Optional<List<Vaga>> findByEstacionamentoId(Integer id) {
		Estacionamento estacionamento = new Estacionamento();
		estacionamento.setId(id);
		
		prepareBuildQuery();
		q.select(root);
		q.where(cb.equal(root.get("estacionamento"), estacionamento));
		
		return Optional.of(getInstance().createQuery(q).getResultList());
	}

	public void remover(Vaga vaga) {
		try {
			getInstance().getTransaction().begin();
				getInstance().remove(vaga);
			getInstance().getTransaction().commit();
			MessageUtils.addMensagemInfo("Vaga removida com sucesso.");
			
		} catch (Exception e) {
			getInstance().getTransaction().rollback();
			MessageUtils.addMensagemWarn("Não foi possivél remover a vaga selecionada.");
		} 
	}
}
