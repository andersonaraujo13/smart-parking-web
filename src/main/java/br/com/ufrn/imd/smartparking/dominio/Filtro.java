package br.com.ufrn.imd.smartparking.dominio;

import java.util.Date;

public class Filtro {
	private Instituicao instituicao;
	private Vinculo vinculo;
	private Estacionamento estacionamento;
	private Vaga vaga;
	private Date dataInicio;
	private Date dataFim;
	public Instituicao getInstituicao() {
		return instituicao;
	}
	public void setInstituicao(Instituicao instituicao) {
		this.instituicao = instituicao;
	}
	public Vinculo getVinculo() {
		return vinculo;
	}
	public void setVinculo(Vinculo vinculo) {
		this.vinculo = vinculo;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date datafim) {
		this.dataFim = datafim;
	}
	public Estacionamento getEstacionamento() {
		return estacionamento;
	}
	public void setEstacionamento(Estacionamento estacionamento) {
		this.estacionamento = estacionamento;
	}
	public Vaga getVaga() {
		return vaga;
	}
	public void setVaga(Vaga vaga) {
		this.vaga = vaga;
	}
	
}
