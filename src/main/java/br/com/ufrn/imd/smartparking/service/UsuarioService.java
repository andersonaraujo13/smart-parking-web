package br.com.ufrn.imd.smartparking.service;

import java.io.Serializable;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.omnifaces.util.Faces;

import br.com.ufrn.imd.smartparking.dao.UsuarioDao;
import br.com.ufrn.imd.smartparking.dominio.Usuario;
import br.com.ufrn.imd.smartparking.utils.MessageUtils;
import br.com.ufrn.imd.smartparking.utils.StringUtils;

@Stateless
public class UsuarioService implements Serializable {

	private static final long serialVersionUID = 8321322071791806697L;
	@Inject
	private UsuarioDao usuarioDao;
	
	public Optional<Usuario> login(String email, String password) {
		String senhaMD5 = StringUtils.encriptMD5(password);
		Optional<Usuario> user = Optional.empty();

		try {
			user = usuarioDao.autenticar(email, senhaMD5);
		} catch (Exception e) {
			MessageUtils.addMensagemWarn("Login ou Senha Incorretas.");
			Faces.getExternalContext().getFlash().setKeepMessages(true);
		}

		return user;
	}

}
