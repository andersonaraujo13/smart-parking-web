package br.com.ufrn.imd.smartparking.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.ufrn.imd.smartparking.dao.VinculoDao;
import br.com.ufrn.imd.smartparking.dominio.Usuario;
import br.com.ufrn.imd.smartparking.dominio.Vinculo;
import br.com.ufrn.imd.smartparking.dominio.VinculoTipo;

@Stateless
public class VinculoService implements Serializable {
	
	private static final long serialVersionUID = -8387813934792706268L;
	
	@Inject
	private VinculoDao vinculoDao;

	public Optional<List<Vinculo>> findVinculoByUserAndTipo(Usuario user, VinculoTipo tipo) {
		
		return vinculoDao.findVinculoByUserAndTipo(user, tipo);
	}

}
