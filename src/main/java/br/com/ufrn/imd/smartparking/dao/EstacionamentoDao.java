package br.com.ufrn.imd.smartparking.dao;

import java.util.List;
import java.util.Optional;

import br.com.ufrn.imd.smartparking.dominio.Estacionamento;
import br.com.ufrn.imd.smartparking.dominio.Instituicao;
import br.com.ufrn.imd.smartparking.utils.MessageUtils;

public class EstacionamentoDao extends GenericDao<Estacionamento> {

	@Override
	public void persist(Estacionamento obj) {
		try {
			getInstance().getTransaction().begin();
				getInstance().persist(obj.getEndereco());
				getInstance().persist(obj);
			getInstance().getTransaction().commit();
		} catch (Exception e) {
			getInstance().getTransaction().rollback();
			e.printStackTrace();
		} 
	}

	public Optional<List<Estacionamento>> findAll() {
		prepareBuildQuery();
		q.select(root);
		q.where(cb.equal(root.get("ativo"), true));
		
		return Optional.of(getInstance().createQuery(q).getResultList());
	}

	@Override
	public void remove(Estacionamento estacionamento) {
		try {
			getInstance().getTransaction().begin();
				getInstance().remove(estacionamento);
			getInstance().getTransaction().commit();
			MessageUtils.addMensagemInfo("Estacionamento removido com sucesso.");
			
		} catch (Exception e) {
			getInstance().getTransaction().rollback();
			MessageUtils.addMensagemWarn("Não foi possivél remover a estacionado selecionado.");
		} 
	}
	
	public Optional<List<Estacionamento>> findByInstituicao(Instituicao instituicao) {
		prepareBuildQuery();
		q.select(root);
		q.where(cb.equal(root.get("instituicao"), instituicao));
		
		return Optional.of(getInstance().createQuery(q).getResultList());
	}
	
	
}
