package br.com.ufrn.imd.smartparking.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.omnifaces.util.Faces;

import br.com.ufrn.imd.smartparking.dao.EstacionamentoDao;
import br.com.ufrn.imd.smartparking.dominio.Estacionamento;
import br.com.ufrn.imd.smartparking.dominio.Usuario;
import br.com.ufrn.imd.smartparking.dominio.Vinculo;
import br.com.ufrn.imd.smartparking.dominio.VinculoTipo;
import br.com.ufrn.imd.smartparking.utils.MessageUtils;

@Stateless
public class EstacionamentoService implements Serializable {

	private static final long serialVersionUID = 8257607570956950743L;
	@Inject
	private EstacionamentoDao estacionamentoDao;
	@Inject
	private VinculoService vinculoService;

	public void save(Estacionamento estacionamento, Vinculo vinculo) throws IOException {
		if(Objects.nonNull(vinculo)) {
			if(vinculo.getVinculo() == VinculoTipo.ADMINISTRADOR) {
				estacionamentoDao.persist(estacionamento);
				MessageUtils.addMensagemInfo("Estacionamento cadastrado com sucesso.");
				Faces.getExternalContext().getFlash().setKeepMessages(true);
				Faces.redirect("admin/estacionamento/index.jsf");
			} else {
				MessageUtils.addMensagemWarn("Você não possuí permissão para realizar essa operação.");
				Faces.getExternalContext().getFlash().setKeepMessages(true);
			}
		} else {
			MessageUtils.addMensagemWarn("Não foi encontrado vinculo para essa instituição.");
			Faces.getExternalContext().getFlash().setKeepMessages(true);
		}
	}

	public List<Estacionamento> findEstacionamentosByUser(Usuario user) {
		Optional<List<Vinculo>> vinculos = vinculoService.findVinculoByUserAndTipo(user, VinculoTipo.ADMINISTRADOR);
		List<Estacionamento> lisEstacionamentos = new ArrayList<>();
		
		if(vinculos.isPresent()) {
			vinculos.get().forEach(vinculo ->{
				List<Estacionamento> list = estacionamentoDao.findByInstituicao(vinculo.getInstituicao()).orElse(new ArrayList<>());
				if(Objects.nonNull(list)) {
					lisEstacionamentos.addAll(list.stream().filter(entity -> entity.isAtivo()).collect(Collectors.toList()));
				}
				
			});
		}
		
		return lisEstacionamentos;
	}

	public List<Estacionamento> findAll() {
		return estacionamentoDao.findAll().get();
	}
	
	public void remove(Estacionamento estacionamento, Vinculo vinculo) throws IOException {
		if(Objects.nonNull(vinculo)) {
			if(vinculo.getVinculo() == VinculoTipo.ADMINISTRADOR) {
				estacionamento.setAtivo(false);
				estacionamentoDao.persist(estacionamento);
				MessageUtils.addMensagemInfo("Estacionamento removido com sucesso.");
				Faces.getExternalContext().getFlash().setKeepMessages(true);
				Faces.redirect("admin/estacionamento/index.jsf");
			} else {
				MessageUtils.addMensagemWarn("Você não possuí permissão para realizar essa operação.");
				Faces.getExternalContext().getFlash().setKeepMessages(true);
			}
		} else {
			MessageUtils.addMensagemWarn("Não foi encontrado vinculo para essa instituição.");
			Faces.getExternalContext().getFlash().setKeepMessages(true);
		}
	}

}
